### Install rasa 2.8.28

## Step 1: Install peotry

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

## Step 2: Install rasa

```bash
poetry install
```

## Step 3: Install packages

```bash
pip install sentencepiece==0.1.96 transformers==4.10.0
```